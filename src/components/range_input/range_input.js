import './range_input.less';
import {TimeInput} from "../../scripts/timeInput";
import InputMask from 'inputmask';

const MAX_TIME = 1440;
const MIN_TIME = 0;
const MAX_VALUE = 1020;
const MIN_VALUE = 480;
const STEP = 5;
const DIVIDER = '-';

const mask = new InputMask({regex: '((([01][0-9]|2[0-4]):[0-5][0-9])' });

export class RangeInput {
  constructor(parameters) {
    let {container, divider, isVisible, minValue, maxValue} = parameters;
    this.element = container;
    this.min = new TimeInput(minValue || MIN_VALUE);
    this.max = new TimeInput(maxValue || MAX_VALUE);
    this.divider = divider || DIVIDER;
    this.renderToHtml();
    this.changeVisibility.bind(this);
    this.changeVisibility(isVisible);
  }

  render() {
    return `
      <div>
        <div class="${this.inputClass}">
          <input data-time="min" type="text" value="${this.min.time}">
          <span class="text-input__dash">${this.divider}</span>
          <input data-time="max"  type="text" value="${this.max.time}">
        </div>
        <div class="${this.containerClass}">
          <div class="range-input__active"></div>
          <input data-time="min" type="range" min="${MIN_TIME}" max="${MAX_TIME}" step="${STEP}" value="${MIN_VALUE}"/>
          <input data-time="max" type='range' min="${MIN_TIME}" max="${MAX_TIME}" step="${STEP}" value="${MAX_VALUE}"/>
        </div>
      </div>
    `;
  }

  renderToHtml() {
    if (this.element) {
      this.element.innerHTML = this.render();
      this.minMaxInputs = ['min', 'max'].reduce((acc, value) => Object.assign(
        {},
        acc,
        {[value]: this.element.querySelector(`[data-time="${value}"][type="range"]`)}
      ), {});
      this.minMaxTextInputs = ['min', 'max'].reduce((acc, value) => Object.assign(
        {},
        acc,
        {[value]: this.element.querySelector(`[data-time="${value}"][type="text"]`)}
      ), {});
      this.activeLine = this.element.querySelector('.range-input__active');
      this.addEvents();
    }
  }

  get containerClass() {
    return 'range-input';
  }

  get inputClass() {
    return 'text-input';
  }

  setMaxTime(value) {
    this.max.time = value;
    if (this.max.timeNumber <= this.min.timeNumber) {
      this.max.time = this.min.time;
    }
    this.minMaxInputs.max.value = this.max.timeNumber;
    this.minMaxTextInputs.max.value = this.max.time;
    this.updateActiveLine();
  }

  setMinTime(value) {
    this.min.time = value;
    if (this.min.timeNumber >= this.max.timeNumber) {
      this.min.time = this.max.time;
    }
    this.minMaxInputs.min.value = this.min.timeNumber;
    this.minMaxTextInputs.min.value = this.min.time;
    this.updateActiveLine();
  }

  updateActiveLine() {
    /* Добавляем 10px, чтобы не перекрывать скролл при 0 и максимуме */
    this.activeLine.style.width = `calc(${this.activeWidth}% - 10px)`;
    this.activeLine.style.left = `calc(${this.activeLeft}% + 10px)`;
  }

  get activeLeft() {
    return this.min.timeNumber / MAX_TIME * 100;
  }

  get activeWidth() {
    return (this.max.timeNumber - this.min.timeNumber) / MAX_TIME * 100;
  }

  setTime(key, value) {
    return key === 'min'
      ? this.setMinTime(value)
      : this.setMaxTime(value);
  }

  addEvents() {
    const {min: minInput, max: maxInput} = this.minMaxInputs;
    const {min: minTextInput, max: maxTextInput} = this.minMaxTextInputs;
    mask.mask(minTextInput);
    mask.mask(maxTextInput);
    ['min', 'max'].forEach(key => {
      this.minMaxTextInputs[key].addEventListener('input', e => {
        const { value } = e.currentTarget;
        if (!value || value.includes('_')) {
          return;
        }
        this.setTime(key, value);
        if (this[key].time === value) {
          this.minMaxInputs[key].value = this[key].timeNumber;
        } else {
          //error handling here
        }
      });
    });
    minInput.addEventListener('input', e => this.setMinTime(Number(e.currentTarget.value)));
    maxInput.addEventListener('input', e => this.setMaxTime(Number(e.currentTarget.value)));
    this.updateActiveLine();
  }

  set containerClass(className) {
    this.element.firstElementChild.className = className;
  }

  changeVisibility(value) {
    this.containerClass = ['range-container', !value && 'range-container--hidden'].filter(Boolean).join(' ');
  }
}