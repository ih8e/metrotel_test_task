import './checkbox.less';

const defaultState = {
  checked: false,
};

const props = {
  id: 'full',
  onChange: () => {},
};

export class Checkbox {
  constructor(parameters) {
    let {container, checked = defaultState.checked, value, onChange} = parameters;
    this.onChange = onChange || (() => {});
    this.state = {checked};
    this.props = value ? {id: value} : props;
    this.element = container;
    this.renderToHtml();
  }

  renderToHtml() {
    if (this.element) {
      this.element.innerHTML = this.render();
      this.addEvents();
    }
  }

  render() {
    const { id } = this.props;
    const { checked } = this.state;
    return `
      <div class="${this.containerClass}">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 20" width="21" height="20"><path fill="white" stroke="#cccccc" d="M6 1L16 1C18.21 1 20 2.79 20 5L20 15C20 17.21 18.21 19 16 19L6 19C3.79 19 2 17.21 2 15L2 5C2 2.79 3.79 1 6 1Z"/></svg>
        <i class="checkbox__check"></i>
        <label for="${id}">
          <input
            id="${id}"
            type="checkbox"
            ${checked && 'checked' || ''}
          />
         ${id}
        </label>
      </div>
     `;
  }

  get containerClass() {
    return ['checkbox', this.state.checked && 'checkbox--checked'].filter(Boolean).join(' ');
  }

  set containerClass(className) {
    this.element.firstElementChild.className = className;
  }

  addEvents() {
    this.element.querySelector('input').addEventListener('change', (e) => {
      this.state.checked = e.currentTarget.checked;
      this.containerClass = this.containerClass;
      this.onChange(this.state.checked);
    });
  }
}