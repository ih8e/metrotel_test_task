import '../styles/index.less';
import { Checkbox } from "../components/checkbox";
import {RangeInput} from "../components/range_input/range_input";
(function (){
  document.body.classList.add('loaded');
  const weekend = ['Saturday', 'Sunday']; // onInit not including weekend by default

  const weekDays = ['Monday', 'Tuesday', 'Wedndesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  const workingHoursTab = document.querySelector('[data-tab="workingHours"]');
  (function addAll() {
    const element = document.createElement('div');
    element.className = 'b-day b-day--all';
    const checkboxContainer = document.createElement('div');
    const rangeContainer = document.createElement('div');
    checkboxContainer.className = 'b-checkbox';
    rangeContainer.className = 'b-range';
    element.append(checkboxContainer);
    element.append(rangeContainer);
    new RangeInput({container: rangeContainer, isVisible: true});
    checkboxContainer.innerHTML = `<div class="b-checkbox__label">Working hours</div>`;
    workingHoursTab.appendChild(element);
  })();
  weekDays.forEach(day => {
    const element = document.createElement('div');
    element.classList.add('b-day');
    const checkboxContainer = document.createElement('div');
    const rangeContainer = document.createElement('div');
    checkboxContainer.classList.add('b-checkbox');
    rangeContainer.classList.add('b-range');
    const isVisible = !weekend.includes(day);
    element.append(checkboxContainer);
    element.append(rangeContainer);
    const rangeInput = new RangeInput({container: rangeContainer, isVisible});
    new Checkbox({
      checked: isVisible,
      container: checkboxContainer,
      value: day,
      onChange: rangeInput.changeVisibility.bind(rangeInput),
    });
    workingHoursTab.appendChild(element);
  });


  /* TABS */
  const tabs = [
    'profile',
    'notifications',
    'password',
    'workingHours',
    'callSettings',
  ];

  const tabsElementMapping = tabs.reduce((acc, name) => Object.assign(
    {},
    acc,
    {
      [name]: {
        nav: document.querySelector(`[data-set-tab="${name}"]`),
        content: document.querySelector(`[data-tab="${name}"]`),
      }
    }), {});

  const activeTabNavClassName = 'b-tabs__item--active';
  const activeTabContentClassName = 'b-tabs__content--active';
  const allTabNavs = document.querySelectorAll('[data-set-tab]');
  const allTabContents = document.querySelectorAll('[data-tab]');
  const removeActiveClass = className => el => el.classList.remove(className);
  const replaceAllTabsActive = () => {
    allTabNavs.forEach(removeActiveClass(activeTabNavClassName));
    allTabContents.forEach(removeActiveClass(activeTabContentClassName));
  };

  tabs.forEach(name => {
    const { nav, content } = tabsElementMapping[name];
    tabsElementMapping[name].nav.onclick = (e) => {
      replaceAllTabsActive();
      nav.classList.add(activeTabNavClassName);
      content.classList.add(activeTabContentClassName);
    };
  });

  /* TABS */
})();
