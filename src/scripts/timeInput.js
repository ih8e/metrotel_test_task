const MAX_HOURS = 23;
const MAX_MINS = 59;

export class TimeInput {
  constructor(time) {
    this._hours = 0;
    this._minutes = 0;
    this.time = time;
  }

  static parseTimeString(time = '00:00') {
    return time.split(':').map(Number);
  }

  static parseTimeNumber(time = 0) {
    return [Math.floor(time / 60), time % 60];
  }

  static formatValue(value) {
    return value < 10 ? `0${value}` : String(value);
  }

  static isValidMinutes(minutes) {
    return Number.isInteger(minutes) && minutes >= 0 && minutes <= MAX_MINS;
  }

  static isValidHours(hours) {
    return Number.isInteger(hours) && hours >= 0 && hours <= MAX_HOURS;
  }

  get timeNumber() {
    return this._hours * 60 + this._minutes;
  }

  get minutes() {
    return TimeInput.formatValue(this._minutes);
  }

  get hours() {
    return TimeInput.formatValue(this._hours);
  }

  is24(hours, minutes) {
    return hours === 24 && minutes === 0;
  }

  set time(value) {
    let [hours, minutes] = [0, 0];
    switch (typeof value) {
      case 'number':
        [hours, minutes] = TimeInput.parseTimeNumber(value);
        break;
      case 'string':
        [hours, minutes] = TimeInput.parseTimeString(value);
        break;
      default:
        return;
    }
    if (this.is24(hours, minutes) || TimeInput.isValidHours(hours) && TimeInput.isValidMinutes(minutes)) {
      [this._hours, this._minutes] = [hours, minutes];
    }
  }

  get time() {
    return `${this.hours}:${this.minutes}`;
  }
}